call plug#begin()

Plug 'kien/ctrlp.vim'
Plug 'Valloric/YouCompleteMe'
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
Plug 'derekwyatt/vim-fswitch'
Plug 'vim-scripts/headerguard'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'vim-airline/vim-airline'

call plug#end()

"General
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set scrolloff=8
set smartcase
set shortmess+=A
command Wudo w !sudo tee % > /dev/null
" copy and paste
set clipboard+=unnamedplus
"vmap <C-c> "+yi
"vmap <C-x> "+c
"vmap <C-v> c<ESC>"+p
"imap <C-v> <ESC>"+pa

"Numbers
set number
set relativenumber

"Colors
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set background=dark
colorscheme molokai
"highlight Normal guibg=none
"highlight NonText guibg=none

"CPP
map <F4> :FSHere<CR>
map <F5> :HeaderguardAdd<CR>
autocmd FileType cpp imap { {<CR>}<Esc>ko

"Ycm
let g:ycm_confirm_extra_conf = 0
"
" Extra cpp
let g:cpp_class_scope_highlight = 1

" Ctrl P
set wildignore+=*.o,*.swp,*.jpg,*.png,*.d
 
" Airline
let g:airline_powerline_fonts = 1
