set -x LC_ALL en_GB.UTF-8
set -x LANG en_GB.UTF-8
set -x EDITOR nvim
set -x SHELL /bin/bash
set -x LD_LIBRARY_PATH /lib32
